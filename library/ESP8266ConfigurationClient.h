#ifndef ESP8266ConfigurationClient_h
#define ESP8266ConfigurationClient_h

#include "Arduino.h"
#include <ArduinoJson.h>

class ESP8266ConfigurationClient {

  public:
    ESP8266ConfigurationClient(String host, String deviceId);
    JsonObject& getConfiguration();
  private:
    String _host;
    String _deviceId;
};

#endif
