#include <ESP8266ConfigurationClient.h>
#include <ESP8266ConfigurationClient.h>

#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>

 HTTPClient http;

ESP8266ConfigurationClient::ESP8266ConfigurationClient(String host, String deviceId) {
  _host = host;
  _deviceId = deviceId;
}

JsonObject& ESP8266ConfigurationClient::getConfiguration(const size_t bufferSize) {
    Serial.println(_host + "/dh7/configure?deviceId=" + _deviceId);
    //StaticJsonBuffer<200> jsonBuffer;
    DynamicJsonBuffer jsonBuffer(bufferSize);

    http.begin(_host + "/dh7/configure?deviceId=" + _deviceId);
    int httpCode = http.GET();

    if(httpCode > 0) {
        // file found at server
        if(httpCode == HTTP_CODE_OK) {
            String payload = http.getString();

            JsonObject& root = jsonBuffer.parseObject(payload);

            // Test if parsing succeeds.
            if (!root.success()) {
              Serial.println("Parsing config failed");
            } else {
              http.end();
              return root["configuration"];
            }
        }
    } else {
        Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
    }

    http.end();
    return jsonBuffer.createObject();
}
