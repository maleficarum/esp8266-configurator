
ESP8266 Configurator (ALPHA Version)
===================

This module allows you to fetch configuration for an ESP8266 device on network connection. allowing you to centralize the configuration of your deployed devices.

----------

How it works?
-------------

When a device becomes online, the module fetch a configuration stored on the server for a specific device.

#### Install

```bash
$ npm install esp8266-configurator --save
```

#### Initialization

```javascript

const index = require('esp8266-configurator');

index.configure(require('./config.json'));

```

#### Configuration

By now, you only can provide a configuration for your device via json config file as follows:

```json

{
  "dbLocation":"./db/",
  "configuration":[
    {
      "device":{
        "id":"TEST-DEV",
        "alias":"TEST",
        "model":"ESP8266",
        "metadata":"",
        "location":{
          "latitude":0,
          "longitude":0
        }
	    },
      "configuration":[{
        "key":"remote",
        "value":"http://"
      }]
    }
  ]
}


```

Firmware
--------------

#### Include libraries

The module has to be included as any other arduino library (See de 'Downloads' section). After it, you can have to include it and configure the configuration server URL and the device id.

```cpp
//These 3 libraries are needed in order to connect to WIFI, fetch configuration and parse it.

#include <ESP8266ConfigurationClient.h>
#include <ESP8266WiFi.h>
#include <ArduinoJson.h>

```

#### Configure the library

```cpp

ESP8266ConfigurationClient client = ESP8266ConfigurationClient("http://192.168.2.101:3000","TEST-DEV");


```

#### Fetch the configuration

```cpp

JsonObject& json = client.getConfiguration();
const char* mqttServer = json["mqttServer"];
Serial.println(mqttServer);

```

[For more ArduinoJson options go to its documentation](https://github.com/bblanchon/ArduinoJson "ArduinoJson documentation")

#### Full sample

```cpp

#include <ESP8266ConfigurationClient.h>
#include <ESP8266WiFi.h>
#include <ArduinoJson.h>

ESP8266ConfigurationClient client = ESP8266ConfigurationClient("http://192.168.2.101:3000","TEST-DEV");
const char* ssid     = "...";
const char* password = "...";

void setup() {
  // We start by connecting to a WiFi network
  Serial.begin(9600);

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  JsonObject& json = client.getConfiguration();
  const char* mqttServer = json["mqttServer"];
  Serial.println(mqttServer);
}

void loop() {
  // put your main code here, to run repeatedly:

}

```


In development
--------------

By now, you can only provide a json config file with all the device's configuration, but the module will may store the configuration in a in-memory database or in a persisted database. Also the module will provide a web UI to handle the device.
